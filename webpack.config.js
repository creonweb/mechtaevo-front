const merge = require('webpack-merge');
const baseConfig = require('./webpack/webpack.base.js');
const serverConfig = require('./webpack/webpack.server.js');
const staticConfig = require('./webpack/webpack.static.js');
const prodConfig = require('./webpack/webpack.prod.js');

const IS_MODERN = true;

module.exports = (env, argv) => {
    const isDev = argv.mode === 'development';
    const isAnalyze = env && env.anlz;
    const isDevServer = env && env.server;

    if (isDevServer) {
        return merge.smart(baseConfig(IS_MODERN), serverConfig());
    }

    // eslint-disable-next-line no-console
    console.log(isDev ? '\nDEVELOPMENT BUILD\n' : '\nPRODUCTION BUILD\n');

    return [
        merge.smart(
            baseConfig(!IS_MODERN, true, argv.watch),
            staticConfig(isDev, isAnalyze, !IS_MODERN),
            !isDev && prodConfig()
        ),
    ];
};
