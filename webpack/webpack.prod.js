const TerserPlugin = require('terser-webpack-plugin');

module.exports = (env) => {
    return {
        optimization: {
            minimizer: [
                new TerserPlugin({
                    test: /\.m?js(\?.*)?$/i,
                    sourceMap: true,
                    cache: true,
                    parallel: true,
                    terserOptions: {
                        compress: {
                            toplevel: true,
                            passes: 3,
                        },
                    },
                }),
            ],
        },
    };
};
