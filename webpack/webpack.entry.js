// All keys must be added in quotes for consistency

module.exports = {
    app: 'index.js',
    init: 'init.js',
    jq: 'mechtaevo.jq.js'
};
