const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const config = require('../assets/config');

module.exports = (isDev, isAnlz, isModern = false) => ({
    output: {
        //filename: isModern ? '[name].[contenthash].mjs' : '[name].[contenthash].js' // For Prod
        filename: isModern ? '[name].mjs' : '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            importLoaders: 1
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            importLoaders: 2
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                ]
            }
        ]
    },
    plugins: [
        new ManifestPlugin({
            fileName: isModern ? 'modern.manifest.json' : 'manifest.json',
            filter: ({ name }) => {
                if (isModern) {
                    return name.endsWith('.mjs')
                } else {
                    return !name.endsWith('.map')
                }
            }
        }),
        new MiniCssExtractPlugin({
            //filename: '[name].[contenthash].css' // For Prod
            filename: '[name].css'
        }),
        isAnlz ? new BundleAnalyzerPlugin({ analyzerPort: isModern ? 8889 : 8890 }) : false
    ].filter(Boolean)
});
