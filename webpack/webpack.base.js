const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const entry = require('./webpack.entry.js');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const babelConfig = require('../babel.config.js');
const _ = require('lodash');
const FileManagerPlugin = require('filemanager-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const pages = require('../views/pages');

const htmlPlugins = pages.map((page) => {
    return new HtmlWebpackPlugin({
        template: `views/${page.name}.hbs`,
        filename: `../${page.name}.html`,
        alwaysWriteToDisk: true,
        inject: (typeof page.useChunks === 'boolean' && page.useChunks) || true,
        chunks: typeof page.useChunks === 'boolean' && page.useChunks ? 'all' : '',
    });
});

module.exports = (isModern, shouldClean = true, isWatch = false) => ({
    devtool: 'source-map',
    entry,
    output: {
        path: path.resolve(__dirname, '../wwwroot/dist'),
        filename: '[name].js',
        jsonpFunction: 'webpackJsonpMechtaevo',
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        modules: [
            'node_modules',
            path.resolve(__dirname, '../'),
            path.resolve(__dirname, '../assets'),
        ],
        alias: {
            'style-settings': path.resolve(
                __dirname,
                '../assets/project/general/scss/settings/index.scss'
            ),
        },
    },
    module: {
        rules: [
            {
                test: /\.(jsx?)$/,
                exclude: [/node_modules/],
                loaders: [
                    {
                        loader: 'babel-loader',
                        options: _.merge(babelConfig, {
                            presets: [
                                [
                                    '@babel/preset-env',
                                    {
                                        targets: {
                                            esmodules: isModern,
                                        },
                                        bugfixes: isModern,
                                    },
                                ],
                            ],
                        }),
                    },
                ],
            },
            {
                test: /\.(tsx?)$/,
                exclude: [/node_modules(?!(\/|\\)@deleteagency)/],
                loaders: [
                    {
                        loader: 'babel-loader',
                        options: _.merge(babelConfig, {
                            presets: [
                                [
                                    '@babel/preset-env',
                                    {
                                        targets: {
                                            esmodules: isModern,
                                        },
                                        bugfixes: isModern,
                                    },
                                ],
                            ],
                        }),
                    },
                    {
                        loader: 'thread-loader',
                        options: {
                            // there should be 1 cpu for the fork-ts-checker-webpack-plugin
                            workers: require('os').cpus().length - 1,
                            poolTimeout: isWatch ? Infinity : 500, // set this to Infinity in watch mode - see https://github.com/webpack-contrib/thread-loader
                        },
                    },
                    {
                        loader: 'ts-loader',
                        options: {
                            happyPackMode: true, // for thread-loader
                        },
                    },
                ],
            },
            {
                test: /\.hbs$/,
                loader: 'handlebars-loader',
                options: {
                    helperDirs: path.resolve(__dirname, '../assets/project/hbs-helpers'),
                    precompileOptions: {
                        knownHelpersOnly: false,
                    },
                },
            },
            {
                test: /\.(png|svg|jpe?g|gif)$/,
                exclude: /svg[\/\\]/,
                loader: 'file-loader',
                options: {
                    name: 'images/[path][name].[ext]',
                },
            },
            {
                test: /\.(woff2?|eot|ttf)$/,
                loader: 'file-loader',
                options: {
                    name: 'fonts/[name].[ext]',
                },
            },
            {
                test: /\.svg$/,
                include: /svg[\/\\]/,
                use: [
                    {
                        loader: 'svg-sprite-loader',
                        options: {
                            symbolId: 'icon-[name]',
                            extract: false,
                        },
                    },
                    {
                        loader: 'svgo-loader',
                        options: {
                            plugins: [
                                { removeNonInheritableGroupAttrs: true },
                                { collapseGroups: true },
                                { removeAttrs: { attrs: '(fill|stroke)' } },
                            ],
                        },
                    },
                ],
            },
        ],
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            automaticNameDelimiter: '.',
            cacheGroups: {
                commons: {
                    chunks: 'initial',
                    name: 'commons',
                    minChunks: 2,
                },
            },
        },
        runtimeChunk: {
            name: 'commons',
        },
    },
    plugins: [
        // set correct path to your /dist folder
        shouldClean
            ? new CleanWebpackPlugin({
                  cleanAfterEveryBuildPatterns: ['!fonts/*'],
              })
            : false,
        new ForkTsCheckerWebpackPlugin({ checkSyntacticErrors: true }),
        new SpriteLoaderPlugin({
            plainSprite: true,
            spriteAttrs: {
                width: '0',
                height: '0',
                focusable: 'false',
                'aria-hidden': 'true',
                class: 'visually-hidden',
            },
        }),
        new FileManagerPlugin({
            onEnd: {
                copy: [
                    {
                        source: path.resolve(__dirname, '../assets/images/'),
                        destination: path.resolve(__dirname, '../wwwroot/images'),
                    },
                    {
                        source: path.resolve(__dirname, '../assets/project/favicons'),
                        destination: path.resolve(__dirname, '../wwwroot/dist/favicons'),
                    },
                ],
            },
        }),
        ...htmlPlugins,
    ].filter(Boolean),
    stats: {
        children: false,
    },
});
