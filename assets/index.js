import 'project/general/css/normalize.css';

/* general styles */

import 'project/general/scss/index.scss';

/* project */

import 'project/lazysizes';

/* plugins */

import 'project/general/js/what-input';
import 'project/fsLightBox';

/* components */
import 'components/header';
import 'components/form';
import 'components/footer';
import 'components/nav';
import 'components/submenu';
import 'components/social-share';
import 'components/banner';
import 'components/slider';
import 'components/section';
import 'components/text-block';
import 'components/card';
import 'components/key-info';
import 'components/accordion';
import 'components/image-card';
import 'components/hero';
import 'components/cookie-bar';
import 'components/map';
import 'components/modal-trigger';
import 'components/order-phone-form';
import 'components/search-form';
import 'components/form-range';
import 'components/form-select';
import 'components/multi-level-nav';
import 'components/mobile-menu-trigger';
import 'components/filter-modal-trigger';
import 'components/filters-modal';
import 'components/autocomplete';
import 'components/tabs';
import 'components/floor-card';
import 'components/price-block';
import 'components/info-block';
import 'components/breadcrumb';
import 'components/project-grid';
import 'components/filter-buttons';
import 'components/pagination';
import 'components/tags';
import 'components/review-card';
import 'components/reference';
import 'components/load-more';
import 'components/content-block';
import 'components/contacts-map';
import 'components/requisites';
import 'components/simple-form';
import 'components/vacancy-info';
import 'components/vacancy-notification';
import 'components/credit-info';
import 'components/chevron-list';
import 'components/credit-table';
import 'components/bank-info-list';
import 'components/employee';
import 'components/team';
import 'components/construction-map';
import 'components/construction-grid';
import 'components/department-scroller';
import 'components/shitcode';

/* require svg */

const files = require.context('./project/general/svg', true, /^\.\/.*\.svg/);

files.keys().forEach(files);

// do not focus sprite in IE

const spriteNode = document.getElementById('__SVG_SPRITE_NODE__');

if (spriteNode) {
    spriteNode.setAttribute('focusable', 'false');

    spriteNode.setAttribute('aria-hidden', 'true');
}
