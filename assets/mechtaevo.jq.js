
$(document).on('pdopage_load', function(e, config, response) {
    window.refreshFsLightbox();
});

$(function(){
    var cookies = localStorage.getItem('cookies');
    if(!cookies) {
        $('#cookies').hide();
    } else {
        $('#cookies').remove();
    }
    $(document).on('click', '#cookies-accept', function(e){
        e.preventDefault();
        //alert('SDF!');
        $('#cookies').remove();
        localStorage.setItem('cookies', true);
    })

    $('.promo-november__close').on('click', function () {
        $('.promo-november').hide();
    })

});

// header
var header = $('header.header');
if(header.length && $(window).width() < 769) {
    var lastScrollTop2 = 0;
    var offset = header.height() / 2;
    var home = $('.page-1');
    $(window).on('scroll', function (event) {
        var st = $(this).scrollTop();
        // down
        if (st > lastScrollTop2) {
            // console.log('down');
            header.css({'opacity':1});
            // up
        } else {
            // console.log('up');
            if (st < offset) {
                header.css({'opacity':1});
            } else {
                header.css({'opacity':0});
            }
        }
        lastScrollTop2 = st;
    });
}

// input mask
$(function(){
    $("input[name='email']").inputmask({
        //mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                casing: "lower"
            }
        }
    });


    $("input[name='phone']").inputmask("(999) 999-99-99", {placeholder: " "});

    $("input[name='phone']").on("change paste keyup", function() {
        var tel = $(this).val();
        var num = tel.replace(/[^\d]/g, '');
        if(num.length > 11 ){
            var re = new RegExp("^(78|77)");
        }

        if (re.test(num)) {
            var nevnum = num.replace(/^(78|77)/,7);
        } else {
            var nevnum = num.substring(0, num.length - 1);
        }

        $(this).val(nevnum).inputmask("(999) 999-99-99", {placeholder: " "});

    });

});

// $(document).on('submit', 'form', function(event) {
//     if(this.getAttribute('id') !== null) {
//         var formData = new FormData(this);
//         console.log(formData);
//         // Отправка в amoCRM
//         var xhr = new XMLHttpRequest();
//         xhr.open("POST", "/assets/components/mechtaevo/amo.php");
//         xhr.setRequestHeader("X_REQUESTED_WITH", "XMLHttpRequest");
//         xhr.send(formData);
//         xhr.onreadystatechange = function() { if (this.readyState != 4) return; };
//     }
// });

$(document).on('af_complete', function(event, response) {

    if(response.success) {
        location.href = location.origin + '/success';
    }
});


// Favorites
$('.js-favorites').on('submit', function(e){
    e.preventDefault();
    e.stopPropagation();

    let form = $(this);
    form.on('submit')
    let iconSVG = form.find('svg.icon');
    let iconIMG = form.find('img.icon');
    // form.removeClass('js-favorites').addClass('disabled');
    let favorites = +$('#count-favorites').text();

    let scroll = $(window).scrollTop();


    $.ajax('/assets/components/mechtaevo/action.php', {
        method: 'post',
        data: $(this).serializeArray(),
        dataType: 'json',
        success: function (res) {
            console.log(res);
            if(res.success) {
                AjaxForm.Message.success(res.message);
                if(res.object.status) {
                    iconSVG.css('display', 'none');
                    iconIMG.css('display', '');
                    iconIMG.parent().removeClass('btn-primary--grey-color').addClass('btn-primary--red-color');
                    $('#count-favorites').text(favorites + 1);
                } else {
                    iconSVG.css('display', '');
                    iconIMG.css('display', 'none');
                    iconIMG.parent().removeClass('btn-primary--red-color').addClass('btn-primary--grey-color');
                    $('#count-favorites').text(favorites - 1);
                }
            } else {
                AjaxForm.Message.error(res.message);
            }
            $(window).scrollTop(scroll);
        },
        error: function (res) {
            res = res.responseJSON;
            console.log(res);
            AjaxForm.Message.error('Упс! Что-то пошло не так.')
        },
    });
    return false;
});
