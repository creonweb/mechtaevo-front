const viewports = {
    mobile: 375,
    tablet: 768,
    desktop: 1024,
    'desktop-wide': 1600,
};

module.exports = {
    viewports,
};
