import BaseComponent from 'project/general/js/baseComponent';
import { eventBus } from 'project/event-bus';
import { eventNames } from 'project/event-bus/js/enums';
import { pageLocker } from 'project/general/js/pageLocker';

export default class FilterModalComponent extends BaseComponent {
    constructor(...args) {
        super(...args);
    }

    static getNamespace() {
        return 'filters-modal';
    }

    onInit() {
        console.log('init filter mocal');
        eventBus.addListener(eventNames.EVENT_SHOW_FILTERS, this.showFilters);
    }

    showFilters = () => {
        pageLocker.lock('filters');
        this.el.classList.add('is-opened');
    }

    closeFilters = () => {
        pageLocker.unlock('filters');
        this.el.classList.remove('is-opened');
    }
}
