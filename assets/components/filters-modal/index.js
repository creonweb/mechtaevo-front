import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import FiltersComponent from './js/filters-modal.component';

BaseCore.register(FiltersComponent);
