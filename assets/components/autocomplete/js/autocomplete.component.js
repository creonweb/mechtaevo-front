import BaseComponent from 'project/general/js/baseComponent';
import AutoComplete from '@tarekraafat/autocomplete.js/dist/js/autoComplete';
import testJson from './data.json';

export default class Autocomplete extends BaseComponent {
    constructor(element) {
        super(element);
        this.extendedSettings = this.getElementAttributeAsObject(this.el, 'data-options') ?? {};
        this.defaultSettings = {};
        this.settings = { ...this.defaultSettings, ...this.extendedSettings };
        this.autoCompleteInstance = null;
        this.listResultText = null;
        this.inputRef = this.el.querySelector('[data-autocomplete-ref="input"]');
        this.closeBtnRef = this.el.querySelector('[data-autocomplete-ref="close"]');
    }

    static getNamespace() {
        return 'autocomplete';
    }

    onInit() {
        this.autoCompleteInstance = new AutoComplete({
            data: {
                src: async () => {
                    const data = testJson;
                    return data;
                },
                key: ['title'],
                cache: false
            },
            placeHolder: 'Поиск',
            selector: () => this.inputRef,
            threshold: 2,
            debounce: 150,
            searchEngine: 'strict',
            resultsList: {
                render: true,
                container: source => {
                    source.setAttribute('class', 'autocomplete__list');
                    source.setAttribute('data-autocomplete-ref', 'list');
                },
                destination: this.el,
                position: 'beforeend',
                element: 'ul'
            },
            maxResults: 4,
            highlight: true,
            resultItem: {
                content: (data, source) => {
                    source.setAttribute('class', 'autocomplete__list-item');
                    source.innerHTML = `<a class="autocomplete__list-link" href="${data.value.url}">${data.match}</a>`;
                    if (!this.listResultText) {
                        this.listResultText = document.createElement('li');
                        this.listResultText.setAttribute('class', 'autocomplete__list-item autocomplete__list-item--message');
                        this.listResultText.innerHTML = 'По вашему запросу найдено';
                        this.list = this.el.querySelector('[data-autocomplete-ref="list"]');
                        this.list.appendChild(this.listResultText);
                    } else {
                        this.list.appendChild(this.listResultText);
                    }
                },
                element: 'li'
            },
            noResults: () => {
                const result = document.createElement('li');
                result.setAttribute('class', 'autocomplete__list-item autocomplete__list-item--no-results');
                result.setAttribute('tabindex', '1');
                result.innerHTML = 'Нет результатов';
                this.list = this.el.querySelector('[data-autocomplete-ref="list"]');
                this.list.appendChild(result);
            },
            onSelection: data => {
                window.location.href = data.selection.value.url;
            }

        });
        this.isInited = true;

        this.addListener(this.closeBtnRef, 'click', this.clearInput);
        this.addListeners();
    }

    clearInput = () => {
        this.inputRef.value = '';
        this.list = this.el.querySelector('[data-autocomplete-ref="list"]');
        this.list.innerHTML = '';
    }

    addListeners = () => {
        ['focus', 'blur'].forEach((eventType) => {
            this.list = this.el.querySelector('[data-autocomplete-ref="list"]');
            this.inputRef.addEventListener(eventType, () => {
                if (eventType === 'blur') {
                    this.list.style.display = 'none';
                } else if (eventType === 'focus') {
                    this.list.style.display = 'block';
                }
            });
        });
    }

    onDestroy() { }
}
