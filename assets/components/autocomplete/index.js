import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import AutocompleteComponent from './js/autocomplete.component';

BaseCore.register(AutocompleteComponent);
