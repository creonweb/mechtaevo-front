import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import FormRange from './js/form-range.component';

BaseCore.register(FormRange);
