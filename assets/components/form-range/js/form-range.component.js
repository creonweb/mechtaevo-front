import noUiSlider from 'nouislider';
import BaseComponent from 'project/general/js/baseComponent';
import 'nouislider/distribute/nouislider.css';

export default class FormRange extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.rangeSlider = null;
        this.rangeEl = this.el.querySelector('[data-range-ref="range"]');
        this.rangeInput = this.el.querySelector('[data-range-ref="input"]');
        this.minValueNode = this.el.querySelector('[data-range-ref="range-min"]');
        this.maxValueNode = this.el.querySelector('[data-range-ref="range-max"]');
        this.defaultOptions = {
            start: [80, 400],
            connect: true,
            step: 10,
            margin: 10,
            range: {
                min: 0,
                max: 600
            },
        };

        this.extendedOptions = this.getElementAttributeAsObject(this.el, 'data-options') ?? {};
        this.options = { ...this.defaultOptions, ...this.extendedOptions };
    }

    static getNamespace() {
        return 'range';
    }

    onInit = () => {
        try {
            if (!this.rangeSlider) {
                this.rangeSlider = noUiSlider.create(this.rangeEl, this.options);
                this.rangeSlider.on('update', (values, handle) => {
                    (handle ? this.maxValueNode : this.minValueNode).innerHTML = parseInt(values[handle], 10).toLocaleString('ru-RU');
                    if (this.rangeInput) {
                        this.rangeInput.value = [parseInt(values[0], 10), parseInt(values[1], 10)];
                    }
                });
                this.isInited = true;
            }
        } catch (error) {
            console.error(error);
        }
    };

    onDestroy() {
        this.rangeSlider.off();
        this.rangeSlider.destroy();
        this.isInited = false;
    }
}
