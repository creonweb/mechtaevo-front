import BaseComponent from 'project/general/js/baseComponent';
import Scroller from 'project/general/js/scroller';

export default class DepartmentScroller extends BaseComponent {
    static getNamespace() {
        return 'department-scroller';
    }

    onInit = () => {
        this.triggers = [...this.el.querySelectorAll('[data-department-scroller-ref="link"]')];
        this.triggers.forEach((item) => this.addListener(item, 'click', e => this.onLinkClick(item, e)));
    };

    onLinkClick = (item) => {
        const selector = item.getAttribute('data-target');
        const el = document.querySelector(`#${selector}`);
        const scroller = new Scroller(el, { scrollOffset: 115 });
        scroller.scrollToTop();
        this.triggers.forEach((link) => link.classList.remove('is-active'));
        item.classList.add('is-active');
    }
}
