import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import ContactsMap from './js/contacts-map.component';

BaseCore.register(ContactsMap);
