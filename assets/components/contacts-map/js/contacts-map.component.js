import BaseComponent from 'project/general/js/baseComponent';
import pinImage from '../img/pin.png';

export default class ContactsMap extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.map = null;
        this.mapContainer = this.el.querySelector('[data-contacts-map-ref="container"]');
        this.defaultMapOptions = {
            center: [55.76, 37.64],
            zoom: 16,
            controls: ['zoomControl'],
        };
        this.extendedOptions = this.getElementAttributeAsObject(this.el, 'data-options') ?? {};
        this.options = { ...this.defaultMapOptions, ...this.extendedOptions };
    }

    static getNamespace() {
        return 'contacts-map';
    }

    onInit = () => {
        window.ymaps.ready(this.initMap);
    };

    initMap = () => {
        this.map = new window.ymaps.Map(this.mapContainer, this.options);
        this.map.behaviors.disable('scrollZoom');
        const myPlacemark = new window.ymaps.Placemark(
            this.map.getCenter(),
            {},
            {
                iconLayout: 'default#image',
                iconImageHref: pinImage,
                iconImageSize: [48, 58],
            }
        );
        this.map.geoObjects.add(myPlacemark);
    };
}
