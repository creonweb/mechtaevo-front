import BaseComponent from 'project/general/js/baseComponent';
import tippy from 'tippy.js';
import 'tippy.js/animations/shift-toward.css';

export default class Nav extends BaseComponent {
    constructor(...args) {
        super(...args);

        this.triggerButtons = [...this.el.querySelectorAll('[data-nav-ref="trigger"]')];
    }

    static getNamespace() {
        return 'nav';
    }

    onInit = () => {
        this.triggerButtons.forEach((btn) => {
            const template = this.el.querySelector('[data-nav-ref="submenu"]');

            tippy(btn, {
                content: template,
                allowHTML: true,
                delay: 100,
                maxWidth: 375,
                appendTo: 'parent',
                animation: 'shift-toward',
                interactive: true,
                duration: [250, 50],
                arrow: false,
                placement: 'bottom-start',
                offset: [-24, -16],
                zIndex: 100,
                onHide: (instance) => {
                    instance.reference.classList.remove('is-active');
                    this.el.classList.remove('is-active');
                },
                onShow: (instance) => {
                    instance.reference.classList.add('is-active');
                    this.el.classList.add('is-active');
                },
            });
        });
    };
}
