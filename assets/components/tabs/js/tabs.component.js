import BaseComponent from 'project/general/js/baseComponent';
import { ArrayHelper } from 'foundation/helpers';
import { deviceObserver } from 'project/general/js/deviceObserver';
import { modalService } from 'project/modal';

const CLASS_ACTIVE = 'is-active';

export default class TabsComponent extends BaseComponent {
    static getNamespace() {
        return 'tabs';
    }

    onInit() {
        this.triggers = [...this.el.querySelectorAll('[data-tabs-ref="trigger"]')];
        this.content = [...this.el.querySelectorAll('[data-tabs-ref="content"]')];
        this.activeTrigger = this.triggers.find((el) => el.classList.contains(CLASS_ACTIVE));
        this.activeContentList =
            this.content.filter((el) => el.classList.contains(CLASS_ACTIVE)) ?? [];
        this.contentById = this.content.reduce((result, el) => {
            const id = el.getAttribute('data-id');
            (result[id] || (result[id] = [])).push(el);
            return result;
        }, {});
        this.modal = null;

        this.options = this.getElementAttributeAsObject(this.el, 'data-options') ?? {};

        this.unsubscribeOnChange = deviceObserver.subscribeOnChange(this._onDeviceTypeChange);
        this._onDeviceTypeChange();
    }

    _onDeviceTypeChange = () => {
        if (
            deviceObserver.is('<', this.options.untilBreakpoint || 'tablet') &&
            this.options.selectMode
        ) {
            this._initMobileView();
        } else {
            this._initTabletView();
        }
    };

    _initMobileView = () => {
        this.removeListeners();
        const modal = document.querySelector(this.options.target);
        this.modal = modalService.create(modal, { ...this.options });

        if (!modal) {
            throw new Error('Can not find modal element');
        }

        const tabsTriggers = [...modal.querySelectorAll('[data-tab-trigger]')];

        tabsTriggers.forEach((el) => {
            const id = el.getAttribute('data-target');
            this.addListener(el, 'click', () => {
                this._showContent(id, this.el.querySelector(`[data-target=${id}]`));
                this.modal.close();
            });
        });
        this.triggers.forEach((el) => {
            this.addListener(el, 'click', () => this.modal.open());
        });
    };

    _initTabletView = () => {
        this.removeListeners();
        this.modal = null;
        this.triggers.forEach((el) => {
            const id = el.getAttribute('data-target');
            this.addListener(el, 'click', (e) => this._onTriggerClick(e, id, el));
            this.addListener(el, 'keydown', (e) => this._onTriggerLeftRight(e, el));
        });
    };

    _onTriggerClick = (e, id, triggerElement) => {
        this._showContent(id, triggerElement);
    };

    _onTriggerMobileClick(el) {}

    _onTriggerLeftRight = (e) => {
        const index = Array.prototype.indexOf.call(this.triggers, e.currentTarget);

        const dir =
            e.which === 37
                ? index - 1
                : e.which === 39
                ? index + 1
                : e.which === 40
                ? 'down'
                : null;

        if (dir !== null) {
            e.preventDefault();
            const nextEl = this.triggers[dir] ?? this.triggers[index];
            const nextId = nextEl.getAttribute('data-target');

            dir === 'down'
                ? this.content[index].focus()
                : this.triggers[dir]
                ? this._showContent(nextId, this.triggers[dir])
                : void 0;
        }
    };

    _showContent(id, triggerElement) {
        const contentToShow = this.contentById[id];
        if (!contentToShow) {
            throw new Error(`No content for id ${id}`);
        }

        if (!ArrayHelper.isEmpty(this.activeContentList)) {
            this.activeContentList.forEach((item) => {
                item.classList.remove(CLASS_ACTIVE);
                item.setAttribute('aria-hidden', 'true');
            });
        }

        if (this.activeTrigger) {
            this.activeTrigger.classList.remove(CLASS_ACTIVE);
            this.activeTrigger.setAttribute('aria-selected', 'false');
            this.activeTrigger.setAttribute('tabindex', '-1');
        }

        if (!ArrayHelper.isEmpty(contentToShow)) {
            contentToShow.forEach((item) => {
                item.classList.add(CLASS_ACTIVE);
                item.removeAttribute('aria-hidden');
            });
        }

        triggerElement.classList.add(CLASS_ACTIVE);
        triggerElement.focus();
        triggerElement.setAttribute('aria-selected', 'true');
        triggerElement.removeAttribute('tabindex');
        this.activeContentList = contentToShow;
        this.activeTrigger = triggerElement;
    }
}
