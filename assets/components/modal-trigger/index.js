import BaseCore from 'project/general/js/baseCore';
import ModalTriggerComponent from './js/modal-trigger.component.js';

BaseCore.register(ModalTriggerComponent);
