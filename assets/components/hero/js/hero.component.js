import { tns } from 'tiny-slider/src/tiny-slider';
import { deviceObserver } from 'project/general/js/deviceObserver';
import BaseComponent from 'project/general/js/baseComponent';

export default class HeroSlider extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.slider = null;
        this.defaultOptions = {
            container: this.el.querySelector('[data-hero-ref="slider"]'),
            items: 1,
            autoplay: false,
            navPosition: 'bottom',
            controls: true,
            loop: true,
            speed: 600,
            mode: 'gallery',
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            preventScrollOnTouch: 'auto'
        };

        this.extendedOptions = this.getElementAttributeAsObject(this.el, 'data-options') ?? {};
        this.options = { ...this.defaultOptions, ...this.extendedOptions };
    }

    static getNamespace() {
        return 'hero';
    }

    onInit = () => {
        this.createCarousel(this.el);
    };

    _onDeviceTypeChange = () => {
        if (deviceObserver.is('<', 'tablet')) {
            this.destroySlider();
        } else {
            this.createCarousel(this.el);
        }
    };

    createCarousel() {
        if (this.slider) {
            this.slider = this.slider.rebuild();
            return;
        }

        this.slider = tns(this.options);
    }

    destroySlider = () => {
        if (this.slider && typeof this.slider.destroy === 'function') {
            this.slider.destroy();
        }
    };

    onDestroy() {
        this.destroySlider();
        this.unsubscribeOnChange();
    }
}
