import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import HeroSlider from './js/hero.component';

BaseCore.register(HeroSlider);
