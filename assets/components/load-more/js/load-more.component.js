import BaseComponent from 'project/general/js/baseComponent';
import anime from 'animejs';

const HIDDEN_CLASS = 'is-hidden';
const DEFAULT_INITIAL_VISIBLE_ITEMS = 8;
const DEFAULT_LOAD_MORE_AMOUNT = 3;

export default class LoadMoreComponent extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.items = [...this.el.querySelectorAll('[data-load-more-ref="item"]')] || [];
        this.showMoreButton = this.el.querySelector('[data-load-more-ref="showMore"]');
        this.showMoreButtonContainer = this.el.querySelector('[data-load-more-ref="showMoreContainer"]');
        this.visibleItemsCount = 0;
        this.defaultOptions = {
            visibleItems: DEFAULT_INITIAL_VISIBLE_ITEMS,
            loadMoreAmount: DEFAULT_LOAD_MORE_AMOUNT
        };

        this.extendedOptions = this.getElementAttributeAsObject(this.el, 'data-options') ?? {};
        this.options = { ...this.defaultOptions, ...this.extendedOptions };
    }

    static getNamespace() {
        return 'load-more';
    }

    onInit() {
        if (this.items.length <= 0) {
            throw new Error('There are should be at least one location to render component');
        }
        this.items.slice(this.options.visibleItems, this.items.length).forEach((item) => {
            item.classList.add(HIDDEN_CLASS);
        });
        if (this.items.length > this.options.visibleItems) {
            this._showMore(this.options.visibleItems);
            this._showButton();
        } else {
            this._showMore(this.items.length);
        }
        this.showMoreButton.addEventListener('click', this._onShowMoreClick);
    }

    _onShowMoreClick = () => {
        this._showMore(this.options.loadMoreAmount);
    };

    _showMore(amount) {
        const extraItems = this._getExtraItems(amount);
        this._showExtraItems(extraItems);
        if (this.visibleItemsCount >= this.items.length) {
            this._hideButton();
        } else {
            this.showMoreButton.setAttribute('aria-expanded', 'true');
        }
    }

    _showExtraItems(extraItems) {
        const notEmptyArray = extraItems && Array.isArray(extraItems) && extraItems.length > 0;
        if (notEmptyArray) {
            extraItems.forEach((item) => {
                item.classList.remove(HIDDEN_CLASS);
            });
            extraItems.forEach((item, index) => {
                anime({
                    targets: item,
                    opacity: [0, 1],
                    translateY: ['20%', '0%'],
                    duration: 200,
                    delay: 50 * index,
                    easing: 'easeOutQuad',
                });
            });
            this.visibleItemsCount += extraItems.length;
        }
    }

    _getExtraItems(amount) {
        return this.items.slice(this.visibleItemsCount, this.visibleItemsCount + amount);
    }

    _showButton() {
        this.showMoreButtonContainer.classList.remove(HIDDEN_CLASS);
        this.showMoreButton.addEventListener('click', this._onshowMoreClick);
    }

    _hideButton() {
        this.showMoreButtonContainer.classList.add(HIDDEN_CLASS);
        this.showMoreButton.removeEventListener('click', this._onShowMoreClick);
    }
}
