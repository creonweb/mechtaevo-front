import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import CookieBar from './js/cookie-bar.component';

BaseCore.register(CookieBar);
