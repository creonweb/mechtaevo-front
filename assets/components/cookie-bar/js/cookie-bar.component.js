import BaseComponent from 'project/general/js/baseComponent';
import Cookies from 'js-cookie';
import anime from 'animejs';

const COOKIE_NAME = 'cookie-policy';
const COOKIE_VALUE = 'setted';
const EASING = 'linear';
const DURATION = 300;

export default class CookieBar extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.acceptBtn = this.el.querySelector('[data-cookie-bar-ref="accept"]');
        this.closeBtn = this.el.querySelector('[data-cookie-bar-ref="close"]');
    }

    static getNamespace() {
        return 'cookie-bar';
    }

    onInit = () => {
        this.addListener(this.acceptBtn, 'click', this._onCookieAccept);
        this.addListener(this.closeBtn, 'click', this._onClose);

        setTimeout(() => {
            if (!this._isCookieSetted()) {
                this.el.style.display = 'block';
                anime({
                    targets: this.el,
                    translateY: ['100%', 0],
                    easing: EASING,
                    duration: DURATION
                });
            }
        }, 1000);
    };

    _onCookieAccept = () => {
        Cookies.set(COOKIE_NAME, COOKIE_VALUE, { expires: 365 });
        this.startCloseAnimation();
    }

    _onClose = () => {
        this.startCloseAnimation();
    }

    startCloseAnimation = () => {
        anime({
            targets: this.el,
            translateY: '100%',
            easing: EASING,
            duration: DURATION,
            opacity: 0,
            complete: () => {
                this.el.style.display = 'none';
            }
        });
    }

    _isCookieSetted() {
        return Cookies.get('cookie-policy') === COOKIE_VALUE;
    }
}
