import BaseComponent from 'project/general/js/baseComponent';
import Scroller from 'project/general/js/scroller';

export default class Team extends BaseComponent {
    static getNamespace() {
        return 'team';
    }

    onInit = () => {
        this.triggers = [...this.el.querySelectorAll('[data-team-ref="link"]')];
        this.sections = [...this.el.querySelectorAll('[data-team-ref="department"]')];

        this.triggers.forEach((item) => this.addListener(item, 'click', e => this.onLinkClick(item, e)));
        const options = {
            rootMargin: '-80px'
        };
        const observer = new IntersectionObserver((entries) => {
            entries.forEach(entry => {
                const targetLink = this.triggers.find(el => el.getAttribute('data-target') === entry.target.getAttribute('id'));
                if (entry.isIntersecting) {
                    targetLink.classList.add('is-active');
                } else {
                    targetLink.classList.remove('is-active');
                }
            });
        }, options);

        this.sections.forEach(el => {
            observer.observe(el);
        });
    };

    onLinkClick = (item) => {
        const selector = item.getAttribute('data-target');
        const el = this.el.querySelector(`#${selector}`);
        const scroller = new Scroller(el, { scrollOffset: 80 });
        scroller.scrollToTop();
        this.triggers.forEach((link) => link.classList.remove('is-active'));
        item.classList.add('is-active');
    }
}
