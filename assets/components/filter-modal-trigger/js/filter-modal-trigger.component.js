import BaseComponent from 'project/general/js/baseComponent';
import { eventBus } from 'project/event-bus';
import { eventNames } from 'project/event-bus/js/enums';

class FilterModalTriggerComponent extends BaseComponent {
    static getNamespace() {
        return 'filter-modal-trigger';
    }

    onInit() {
        this.addListener(this.el, 'click', this.onClick);
    }

    onDestroy() {}

    onClick = (e) => {
        e.preventDefault();
        eventBus.emit(eventNames.EVENT_SHOW_FILTERS);
    };
}

export default FilterModalTriggerComponent;
