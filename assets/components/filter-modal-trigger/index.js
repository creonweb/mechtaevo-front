import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import FilterModalTriggerComponent from './js/filter-modal-trigger.component';

BaseCore.register(FilterModalTriggerComponent);
