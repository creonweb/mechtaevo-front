import BaseComponent from 'project/general/js/baseComponent';
import { deviceObserver } from 'project/general/js/deviceObserver';
import {
    setContentToState, collapse, expand, resetContentStyles
} from './accordion.helpers';

const CLASS_ACTIVE = 'is-active';

export default class Accordion extends BaseComponent {
    constructor(element) {
        super(element);
        this._isAnimating = false;
        this._isExpanded = false;
        if (this.el.classList.contains('is-active')) {
            this._isExpanded = true;
        }
        this.defaultOptions = {};

        this.contentRef = this.el.querySelector('[data-accordion-ref="content"]');
        this.triggerRef = this.el.querySelector('[data-accordion-ref="trigger"]');
        this.extendedOptions = this.getElementAttributeAsObject(this.el, 'data-options') ?? {};
        this.options = { ...this.defaultOptions, ...this.extendedOptions };
    }

    static getNamespace() {
        return 'accordion';
    }

    onInit() {
        this.unsubscribeOnChange = deviceObserver.subscribeOnChange(this._onDeviceTypeChange);
        this.initAccordion();
    }

    initAccordion = () => {
        if (this.options.mobileOnly && deviceObserver.is('>=', 'tablet')) {
            return;
        }
        setContentToState(this.contentRef, this._isExpanded);
        this.setAccessibilityState(this._isExpanded);
        this._addListeners();
    }

    _addListeners() {
        this.addListener(this.triggerRef, 'click', this._onClickTrigger);
    }

    _onDeviceTypeChange = () => {
        if (!this.options.mobileOnly) {
            return;
        }
        if (this.options.mobileOnly && deviceObserver.is('<', 'tablet')) {
            this.initAccordion();
        } else {
            this.destroy();
        }
    }

    _onClickTrigger = (event) => {
        if (typeof event !== 'undefined' && typeof event.preventDefault === 'function') {
            event.preventDefault();
        }

        if (this._isAnimating) return;

        if (this._isExpanded) {
            this.collapse();
        } else {
            this.expand();
        }
    };

    expand = () => {
        this._isAnimating = true;
        this.el.classList.add(CLASS_ACTIVE);
        this.triggerRef.classList.add(CLASS_ACTIVE);
        expand(this.contentRef, this._onAnimationComplete);
    };

    collapse = () => {
        this._isAnimating = true;
        this.el.classList.remove(CLASS_ACTIVE);
        this.triggerRef.classList.remove(CLASS_ACTIVE);
        collapse(this.contentRef, this._onAnimationComplete);
    };

    _onAnimationComplete = () => {
        this._isAnimating = false;
        this._isExpanded = !this._isExpanded;
        this.setAccessibilityState(this._isExpanded);
    };

    setAccessibilityState = (isExpanded) => {
        this.triggerRef.setAttribute('aria-expanded', Boolean(isExpanded).toString());
        this.contentRef.setAttribute('aria-hidden', !isExpanded);
    };

    onDestroy() {
        resetContentStyles(this.contentRef);
    }
}
