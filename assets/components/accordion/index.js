import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import AccordionComponent from './js/accordion.component';

BaseCore.register(AccordionComponent);
