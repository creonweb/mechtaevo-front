import BaseComponent from '../../../project/general/js/baseComponent';
import data from './data.json';
import pinImage from '../img/pin.png';
import pinImageActive from '../img/pin-active.png';

export default class Map extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.map = null;
        this.mapContainer = this.el.querySelector('[data-map-ref="container"]');
    }

    static getNamespace() {
        return 'map';
    }

    onInit = () => {
        window.ymaps.ready(this.initMap);
    };

    initMap = () => {
        this.map = new window.ymaps.Map(this.mapContainer, {
            center: [55.76, 37.64],
            zoom: 7,
            controls: ['zoomControl']
        });
        this.map.behaviors.disable('scrollZoom');

        const myCollection = new window.ymaps.GeoObjectCollection({}, {
        });

        const ballonContent = window.ymaps.templateLayoutFactory.createClass(
            `
                <div class="map-popup">
                    <div class="map-popup__inner">
                        <div class="map-popup__media">
                            <img src="$[properties.balloonImageSrc]" alt="image">
                        </div>
                        <a class="map-popup__btn" href="$[properties.balloonUrl]">
                            $[properties.balloonLinkText]
                        </a>
                        <div class="hr initial-m-15"></div>
                        <p class="map-popup__address">
                          $[properties.balloonAddress]
                        </p>
                    </div>
                </div>
            `
        );

        for (let i = 0; i < data.length; i++) {
            myCollection.add(
                new window.ymaps.Placemark(data[i].coordinates, data[i].properties, {
                    iconLayout: 'default#image',
                    iconImageHref: pinImage,
                    iconImageSize: [48, 58],
                    balloonShadow: false,
                    balloonMaxWidth: 195,
                    balloonMinWidth: 195,
                    balloonCloseButton: false,
                    balloonContentLayout: ballonContent,
                    balloonPanelMaxMapArea: 0,
                    hideIconOnBalloonOpen: false,
                    balloonOffset: [-280, 130]
                })
            );
        }

        myCollection.events.add('click', (e) => {
            e.get('target').options.set('iconImageHref', pinImageActive);
        });

        myCollection.events.add('balloonclose', (e) => {
            e.get('target').options.set('iconImageHref', pinImage);
        });

        this.map.geoObjects.add(myCollection);
        this.map.setBounds(myCollection.getBounds());

        this.map.events.add('click', () => {
            if (this.map.balloon.isOpen()) {
                this.map.balloon.close();
            }
        });
    }
}
