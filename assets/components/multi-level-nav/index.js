import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import MultiLevelNavComponent from './js/multi-level-nav.component';

BaseCore.register(MultiLevelNavComponent);
