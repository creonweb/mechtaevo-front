import keyBy from 'lodash/keyBy';
import BaseComponent from 'project/general/js/baseComponent';
import anime from 'animejs';
import { eventBus } from 'project/event-bus';
import { eventNames } from 'project/event-bus/js/enums';
import { pageLocker } from 'project/general/js/pageLocker';

const DURATION = 300;
const EASING = 'linear';

export default class MultiLevelNavComponent extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.root = this.el.querySelector('[data-multi-level-nav-ref="root"]');
        this.close = this.el.querySelector('[data-multi-level-nav-ref="close"]');
        this.triggers = [...this.el.querySelectorAll('[data-multi-level-nav-ref="triggers"]')];
        this.items = [...this.el.querySelectorAll('[data-multi-level-nav-ref="items"]')];
        this.back = [...this.el.querySelectorAll('[data-multi-level-nav-ref="back"]')];
    }

    static getNamespace() {
        return 'multi-level-nav';
    }

    onInit() {
        this.stack = [this.root];
        this.itemsById = keyBy(this.items, (el) => el.getAttribute('data-id'));

        this.triggers.forEach((el) => {
            const id = el.getAttribute('data-target');
            this.addListener(el, 'click', this._onTriggerClick.bind(this, id));
        });

        this.back.forEach((el) => this.addListener(el, 'click', this._onBackClick));
        this.addListener(this.close, 'click', this.closeMenu);

        eventBus.addListener(eventNames.EVENT_SHOW_MENU, this.showMenu);
    }

    showMenu = () => {
        pageLocker.lock('menu');
        this.el.classList.add('is-active');
    }

    closeMenu = () => {
        pageLocker.unlock('menu');
        this.el.classList.remove('is-active');
    }

    _onTriggerClick(id) {
        this._showItem(id);
    }

    _showItem(id) {
        return new Promise((resolve, reject) => {
            try {
                const itemToShow = this.itemsById[id];
                if (!itemToShow) {
                    console.warn(`No items for id ${id}`);
                    return;
                }

                const current = this.stack[this.stack.length - 1];

                anime({
                    targets: current,
                    opacity: [1, 0],
                    zIndex: [1, 0],
                    duration: DURATION,
                    easing: EASING,
                });

                itemToShow.style.visibility = 'visible';

                anime({
                    targets: itemToShow,
                    opacity: [0, 1],
                    zIndex: [0, 1],
                    duration: DURATION,
                    easing: EASING,
                    complete: () => {
                        this.stack.push(itemToShow);
                        resolve();
                    },
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    _onBackClick = () => {
        this._goBack();
    };

    _goBack() {
        if (this.stack.length > 1) {
            const item = this.stack.pop();
            anime({
                targets: item,
                opacity: [1, 0],
                zIndex: [1, 0],
                duration: DURATION,
                easing: EASING,
                complete: () => {
                    item.style.visibility = 'hidden';
                },
            });
        }

        const root = this.stack[this.stack.length - 1];

        root.style.visibility = 'visible';

        anime({
            targets: root,
            opacity: [0, 1],
            zIndex: [0, 1],
            duration: DURATION,
            easing: EASING,
        });
    }
}
