import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import FormSelect from './js/form-select.component';

BaseCore.register(FormSelect);
