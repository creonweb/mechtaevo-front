import BaseComponent from 'project/general/js/baseComponent';
import tippy from 'tippy.js';
import 'tippy.js/animations/shift-toward.css';

export default class FormSelect extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.tippyInstance = null;
        this.selectBtn = this.el.querySelector('[data-select-ref="selectBtn"]');
        this.selectBtnValue = this.el.querySelector('[data-select-ref="selectValue"]');
        this.optionBtns = [...this.el.querySelectorAll('[data-select-ref="optionBtn"]')];
    }

    static getNamespace() {
        return 'select';
    }

    onInit = () => {
        this.isInited = true;
        this.optionBtns.forEach((optionBtn) => {
            this.addListener(optionBtn, 'click', this._onSelectOption);
        });

        const template = this.el.querySelector('[data-select-ref="selectList"]');
        tippy(this.selectBtn, {
            content: template,
            allowHTML: true,
            onCreate: (instance) => {
                this.tippyInstance = instance;
            },
            trigger: 'click',
            delay: 100,
            appendTo: 'parent',
            animation: 'shift-toward',
            interactive: true,
            duration: [250, 150],
            arrow: false,
            placement: 'bottom-start',
            offset: [0, 10],
            zIndex: 100,
            onHide: (instance) => {
                instance.reference.classList.remove('is-active');
                this.el.classList.remove('is-active');
            },
            onShow: (instance) => {
                instance.reference.classList.add('is-active');
                this.el.classList.add('is-active');
            },
        });
    };

    onDestroy() {
        this.isInited = false;
    }

    _onSelectOption = (e) => {
        this.tippyInstance.hide();
        this.selectBtnValue.textContent = e.target.getAttribute('data-value');
    };
}
