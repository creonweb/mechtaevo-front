import BaseCore from 'project/general/js/baseCore';
import MobileMenuTriggerComponent from './js/mobile-menu-trigger.component';

BaseCore.register(MobileMenuTriggerComponent);
