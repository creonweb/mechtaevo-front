import BaseComponent from 'project/general/js/baseComponent';
import { eventBus } from 'project/event-bus';
import { eventNames } from 'project/event-bus/js/enums';

class MobileMenuTriggerComponent extends BaseComponent {
    static getNamespace() {
        return 'mobile-menu-trigger';
    }

    onInit() {
        this.addListener(this.el, 'click', this.onClick);
    }

    onDestroy() {

    }

    onClick = (e) => {
        e.preventDefault();
        eventBus.emit(eventNames.EVENT_SHOW_MENU);
    };
}

export default MobileMenuTriggerComponent;
