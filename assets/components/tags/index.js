import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import TagsComponent from './js/tags.component';

BaseCore.register(TagsComponent);
