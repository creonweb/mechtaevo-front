import BaseComponent from 'project/general/js/baseComponent';
import anime from 'animejs';
import { deviceObserver } from 'project/general/js/deviceObserver';

const HIDDEN_CLASS = 'is-hidden';
const DEFAULT_INITIAL_VISIBLE_ITEMS = 6;

export default class TagsComponent extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.items = [...this.el.querySelectorAll('[data-tags-ref="item"]')] || [];
        this.showAllButton = this.el.querySelector('[data-tags-ref="show-all"]');
    }

    static getNamespace() {
        return 'tags';
    }

    onInit() {
        if (this.items.length <= 0) {
            throw new Error('There are should be at least one location to render component');
        }
        this.unsubscribeOnChange = deviceObserver.subscribeOnChange(this._onDeviceTypeChange);
        this.items.slice(DEFAULT_INITIAL_VISIBLE_ITEMS, this.items.length - 1).forEach((item) => {
            item.classList.add(HIDDEN_CLASS);
        });
        this.showAllButton.addEventListener('click', this._onShowAllClick);
        this._onDeviceTypeChange();
    }

    _onDeviceTypeChange = () => {
        if (deviceObserver.is('<', 'tablet')) {
            this._showMore(this.items.length);
            this._hideButton();
        }
    };

    _onShowAllClick = () => {
        this._showMore(this.items.length);
    };

    _showMore(amount) {
        const extraItems = this._getExtraItems(amount);
        this._showExtraItems(extraItems);
        this._hideButton();
    }

    _showExtraItems(extraItems) {
        const notEmptyArray = extraItems && Array.isArray(extraItems) && extraItems.length > 0;
        if (notEmptyArray) {
            extraItems.forEach((item) => {
                item.classList.remove(HIDDEN_CLASS);
            });
            extraItems.forEach((item, index) => {
                anime({
                    targets: item,
                    opacity: [0, 1],
                    translateY: ['100%', '0%'],
                    duration: 200,
                    delay: 50 * index,
                    easing: 'easeOutQuad',
                });
            });
        }
    }

    _getExtraItems(amount) {
        return this.items.slice(DEFAULT_INITIAL_VISIBLE_ITEMS, amount);
    }

    _hideButton() {
        this.showAllButton.classList.add(HIDDEN_CLASS);
        this.showAllButton.removeEventListener('click', this._onShowAllClick);
    }
}
