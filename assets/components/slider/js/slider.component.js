import { tns } from 'tiny-slider/src/tiny-slider';
import { deviceObserver } from 'project/general/js/deviceObserver';
import BaseComponent from 'project/general/js/baseComponent';

export default class Slider extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.slider = null;
        this.defaultOptions = {
            container: this.el,
            items: 1.1,
            gutter: 16,
            autoplay: true,
            navPosition: 'bottom',
            controls: false,
            loop: true,
            preventScrollOnTouch: 'auto',
            responsive: {
                1024: {
                    items: 3,
                },
            },
        };

        this.extendedOptions = this.getElementAttributeAsObject(this.el, 'data-options') ?? {};
        this.options = { ...this.defaultOptions, ...this.extendedOptions };
    }

    static getNamespace() {
        return 'slider';
    }

    onInit = () => {
        if (this.options.type === 'full') {
            this.createCarousel();
        } else {
            this.unsubscribeOnChange = deviceObserver.subscribeOnChange(this._onDeviceTypeChange);
            if (deviceObserver.is('<', 'tablet')) {
                this.createCarousel();
            }
        }
    };

    _onDeviceTypeChange = () => {
        if (deviceObserver.is('<', 'tablet')) {
            this.createCarousel();
        } else {
            this.destroySlider();
        }
    };

    createCarousel() {
        if (this.slider) {
            this.slider = this.slider.rebuild();
            return;
        }

        this.slider = tns(this.options);
        this.slider.events.on('indexChanged', () => {
            if (window.refreshFsLightbox) {
                window.refreshFsLightbox();
            }
        });
    }

    destroySlider = () => {
        if (this.slider && typeof this.slider.destroy === 'function') {
            this.slider.destroy();
        }
    };

    onDestroy() {
        this.destroySlider();
        this.unsubscribeOnChange();
    }
}
