import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import Slider from './js/slider.component';

BaseCore.register(Slider);
