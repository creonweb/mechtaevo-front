import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import ConstructionMap from './js/construction-map.component';

BaseCore.register(ConstructionMap);
