import './scss/index.scss';
import BaseCore from 'project/general/js/baseCore';
import ProjectGridComponent from './js/project-grid.component';

BaseCore.register(ProjectGridComponent);
