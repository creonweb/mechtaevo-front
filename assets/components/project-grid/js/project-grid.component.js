import { tns } from 'tiny-slider/src/tiny-slider';
import BaseComponent from 'project/general/js/baseComponent';
import { deviceObserver } from 'project/general/js/deviceObserver';

export default class ProjectGrid extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.thumbnailsSlider = null;
        this.thumbnailsSliderEl = this.el.querySelector(
            '[data-project-grid-ref="thumbnails-slider"]'
        );

        this.mobileThumbnailsOptions = {
            container: this.thumbnailsSliderEl,
            items: 2,
            gutter: 16,
            controls: false,
            controlsContainer: '[data-project-grid-ref="controls"]',
            preventScrollOnTouch: 'force',
            navPosition: 'bottom',
        };

        this.desktopThumbnailsOptions = {
            container: this.thumbnailsSliderEl,
            items: 2,
            gutter: 16,
            controls: true,
            controlsContainer: '[data-project-grid-ref="controls"]',
            preventScrollOnTouch: 'force',
            navPosition: 'bottom',
            axis: 'vertical',
            speed: 400
        };
    }

    static getNamespace() {
        return 'project-grid';
    }

    onInit = () => {
        this.unsubscribeOnChange = deviceObserver.subscribeOnChange(this._onDeviceTypeChange);
        this._onDeviceTypeChange();
    };

    _onDeviceTypeChange = () => {
        if (deviceObserver.is('<', 'desktop')) {
            this.createCarousel(this.mobileThumbnailsOptions);
        } else {
            this.createCarousel(this.desktopThumbnailsOptions);
        }
    };

    createCarousel(thumbnailsSliderOptions) {
        if (this.thumbnailsSlider) {
            return;
        }
        this.thumbnailsSlider = tns(thumbnailsSliderOptions);
    }

    destroySlider = () => {
        if (this.thumbnailsSlider && typeof this.thumbnailsSlider.destroy === 'function') {
            this.thumbnailsSlider.destroy();
        }
    };

    onDestroy() {
        this.destroySlider();
        this.unsubscribeOnChange();
    }
}
