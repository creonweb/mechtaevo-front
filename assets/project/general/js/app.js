import nanoid from 'nanoid';
import baseCore from './baseCore';

const pageScrolledOffset = 60;

class App {
    constructor() {
        this.id = nanoid(10);
        this.config = window.appConfig || {};
        this.isReduceMotionEnabled = matchMedia(
            'only screen and (prefers-reduced-motion: reduce)'
        ).matches;

        this.isMobile = 'ontouchstart' in document.documentElement;
        this.isScrolled = false;
    }

    init() {
        baseCore.init(document.body);
        window.addEventListener('scroll', this._onPageScroll);
        this._onPageScroll();
    }

    _onPageScroll = () => {
        const scrollPos = this.isMobile ?
            document.scrollingElement.scrollTop :
            document.documentElement.scrollTop;
        if (scrollPos > pageScrolledOffset && !this.isScrolled) {
            document.documentElement.classList.add('is-scrolled');
            this.isScrolled = true;
        } else if (scrollPos <= pageScrolledOffset && this.isScrolled) {
            document.documentElement.classList.remove('is-scrolled');
            this.isScrolled = false;
        }
    };
}

const instance = new App();
export default instance;
