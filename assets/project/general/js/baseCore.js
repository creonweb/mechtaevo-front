class BaseCore {
    constructor() {
        this.componentClasses = [];
        this.componentInstances = [];
    }

    init(container) {
        this.initComponents(container);
    }

    initComponents(container) {
        this.componentClasses.forEach(({ componentClass }) => {
            this.initComponent(container, componentClass);
        });
    }

    initComponent(containerEl, componentClass) {
        const container = containerEl || document;
        const componentName = componentClass.getNamespace();

        if (typeof componentName !== 'string') {
            throw new Error('tagName is empty', componentClass);
        }

        const componentNodes = [
            ...container.querySelectorAll(`[data-component~="${componentName}"]`),
        ];

        componentNodes.forEach((componentNode) => {
            if (!componentNode.components) {
                componentNode.components = [];
            }

            let allowInit = true;

            componentNode.components.forEach((existingInstance) => {
                if (
                    existingInstance.constructor.getNamespace() === componentName &&
                    existingInstance.isInited
                ) {
                    allowInit = false;
                }
            });

            if (allowInit) {
                const componentInstance = new componentClass(componentNode);
                componentInstance.init();

                componentNode.components.push(componentInstance);
                this.componentInstances.push(componentInstance);
            }
        });
    }

    destroyComponents(container) {
        this.componentInstances = this.componentInstances.filter((instance) => {
            if (instance.el === container || container.contains(instance.el)) {
                instance.destroy();
                if (instance.el) {
                    instance.el.components = instance.el.components.filter((i) => i !== instance);
                }

                return false;
            }
            return true;
        });
    }

    register(componentClass, selector = null) {
        this.componentClasses.push({
            componentClass,
            selector,
        });
    }
}
const baseCore = new BaseCore();
export default baseCore;
