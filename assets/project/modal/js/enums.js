export const bemClasses = Object.freeze({
});

export const eventTypes = Object.freeze({
    OPEN: 'modal:open',
    CLOSE: 'modal:close',
});
