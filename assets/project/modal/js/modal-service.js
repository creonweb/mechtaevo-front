import BaseCore from 'project/general/js/baseCore';
import { pageLocker } from 'project/general/js/pageLocker';
import { generateBemCss } from 'project/helpers';
import ModalService from './modal';

const modalService = new ModalService();

modalService.init({
    autoCloseOnClickOutside: false,
    onModalInit: (modal) => {
        BaseCore.init(modal.element);
    },
    onModalDestroy: (modal) => {
        BaseCore.destroy(modal.element);
    },
    onBeforeFirstModalOpen: () => {
        pageLocker.lock('modal');
    },
    onAfterLastModalClose: () => {
        pageLocker.unlock('modal');
    },
    defaultModalTemplate: ({ bemList, title }) => {
        const rootCss = generateBemCss('modal', bemList);
        return `
        <div data-modal class="${rootCss}" aria-hidden="true" role="dialog">
            <div class="modal__head" data-modal-head>
                <button class="modal__close btn" type="button" data-modal-close>
                    <svg class="icon" width="14" height="14" focusable="false">
                        <use xlink:href="#icon-cross" />
                    </svg>
                </button>
                 ${title ? `<div class="modal__title">${title}</div>` : ''}
            </div>
            <div class="modal__wrapper" data-modal-wrapper>
                <div class="modal__content" data-modal-content></div>
            </div>
        </div>
        `;
    },
});

export default modalService;
