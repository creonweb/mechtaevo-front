"use strict";
/*!
 * handlebars-helper-repeat <https://github.com/helpers/handlebars-helper-repeat>
 *
 * Copyright (c) 2014-2018, Jon Schlinkert.
 * Released under the MIT License.
 */
const isNumber = require('is-number');
module.exports = function () {
    const args = arguments;
    if (args.length > 2) {
        throw new Error(`Expected 0, 1 or 2 arguments, but got ${args.length}`);
    }
    const options = args[args.length - 1];
    const hash = options.hash || {};
    const count = hash.count || args[0] || 0;
    const start = hash.start || 0;
    const step = hash.step || 1;
    const data = { count, start, step };
    if (typeof args[0] === 'string' && !isNumber(args[0]) && args[0] !== '') {
        return repeat(data, args[0]);
    }
    if (data.count > 0) {
        return repeatBlock(data, this, options);
    }
    return options.inverse(this);
};
function repeat({ count, start, step }, thisArg) {
    const max = count * step + start;
    let index = start;
    let str = '';
    while (index < max) {
        str += thisArg;
        index += step;
    }
    return str;
}
function repeatBlock({ count, start, step }, thisArg, options) {
    const max = count * step + start;
    let index = start;
    let str = '';
    do {
        const data = {
            index,
            count,
            start,
            step,
            first: index === start,
            last: index >= max - step
        };
        const blockParams = [index, data];
        str += options.fn(thisArg, { data, blockParams });
        index += data.step;
    } while (index < max);
    return str;
}
//# sourceMappingURL=repeat.js.map