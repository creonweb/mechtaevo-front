export const generateBemCss = (bemName, bemList) => {
    let bemCss = '';
    if (Array.isArray(bemList)) {
        bemCss = bemList
            .filter((bemModifier) => bemModifier)
            .map((bemModifier) => `${bemName}--${bemModifier}`)
            .join(' ');
    }
    return bemCss ? `${bemName} ${bemCss}` : bemName;
};
