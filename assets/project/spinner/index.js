import './scss/index.scss';
import ElementSpinner from './js/element-spinner';
import pageSpinner from './js/page-spinner';

export { ElementSpinner, pageSpinner };
