export default class ArrayHelper {
    static findIndexByProp(array: [], prop: string, value: string | number): number {
        for (let i = 0; i < array.length; i += 1) {
            if (array[i][prop] === value) {
                return i;
            }
        }

        return -1;
    }

    static isEmpty = (array: []): boolean => !Array.isArray(array) || !array.length;
}
